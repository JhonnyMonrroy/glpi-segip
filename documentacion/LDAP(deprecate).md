# Configuración para LDAP
El software nativo del GLPI ofrece un plugin para la conección a un Servidor LDAP, para configurar estos accesios seguir los siguientes pasos.
## Descargar el plugin oficial _LDAP Tools_ para GLPI
### Clave de Registro
#### Paso 1
Es necesario que se registre o tenga una cuenta en la pagina <https://services.glpi-network.com/> para obtener su clave de registro

![](images/ldap001.png)

Autenticarse y acceder a la pagina

![](images/ldap002.png)

#### Paso 2
Una vez autenticado en la pagina inicial ir al apartado *Registro*.

![](images/ldap003.png)

#### Paso 3
En ese apartado se despliega una clave de acceso, copiar esa clave

![](images/ldap004.png)

#### Paso 4
En el sistema GLPI instalado autenticarse con el usuario administrador

![](images/ldap005.png)

#### Paso 5
En el apartado de configuración, hacer clic en la opción *General*

![](images/ldap006.png)

#### Paso 6
En la pantalla que se depliega elegir la opción *GLPI Network*

![](images/ldap007.png)

#### Paso 7
Pegar la clave copiada anteriormente en el campo *Clave de registro* y Guardar.

![](images/ldap008.png)

Al completar este paso se teien que obtener un mensaje de que la clave esta confirmada

![](images/ldap009.png)

### Instalar el plugin para LDAP
#### Paso 1
En el panel del administrador buscar el apartado _Configuración > plugins_.

![](images/ldap010.png)

#### Paso 2
En la barra superior seleccionar la opción *Mercado*.

![](images/ldap011.png)

#### Paso 3
Hacer clic en la pestaña descubrir para listar los plugins disponibles

![](images/ldap012.png)

#### Paso 4
Filtrar las busquedas por palabra colocando el texto *LDAP*, luego hacer click en el plugins *LDAP Tools*.

![](images/ldap013.png)

> **Nota.-** Para descargar el plugins debes tener una cuenta premiun en GLPI.

### Configurar el directorio LDAP en el sistema GLPI

#### Paso 1
En el panel del administrador buscar el apartado _Configuración > Authenticación_..

![](images/ldap014.png)

#### Paso 2
Seleccionar la opción *"Directorios LDAP"*

![](images/ldap015.png)

#### Paso 3
Seleccionamos la opción *"+Añadir"*

![](images/ldap016.png)

#### Paso 4
Llenar los campos con la información del servidor LDAP

![](images/ldap017.png)

#### Paso 5
Si todo salio bien el servidor LDAP fue adicionado correctamente.
A continuación hay que activar el servidor.
Seleccionar el servidor y presionar el boton _"Acciones"_

![](images/ldap018.png)

#### Paso 6

En acciones seleccionar _"Actualizar>Directorios LDAP - Activo>Si"_ y pulsar sobre el boton **Enviar**.

![](images/ldap020.png)

#### Paso 7

### Probar la conexión al servidor LDAP

#### Paso 1

Hacer clic en el nombre del LDAP instalado

![](images/ldap021.png)

#### Paso 2

Colocar los parametros para conexión y busqueda de usuarios.

![](images/ldap022.png)

#### Paso 3
Para probar la conexión, hacer clic en la opción probar

![](images/ldap023.png)

#### Paso 4
Para configurar los campos que se sincronizarán seleccionar los campos adecuados.

![](images/ldap024.png)

> **Nota.-** Si se desea se pueden configurar otros parametros opcionalmente.

### Importar usuarios del LDAP
Para importar los usuarios de su LDAP, siga los siguientes pasos:

#### Paso 1
Ingresar con una cuenta administrador, luego en el menu Administración, seleccionar la opción _Usuarios_.

![](images/ldap025.png)

#### Paso 2
Hacer click en el enlace a directorio LDAP.

![](images/ldap026.png)

#### Paso 3
Hacer click en la opción importar nuevos usuarios.

![](images/ldap027.png)

#### Paso 4
Colocar en los campos el dato que se quiere filtrar de la busqueda y presionar el boton _Buscar_.

![](images/ldap028.png)

#### Paso 5
Seleccionar a los usuarios que se desee importar, presionar el boton _Acciones_.

![](images/ldap029.png)

#### Paso 6
El el submenu Acciones, seleccióna la opción importar y luego hacer click en el boton _Enviar_.

![](images/ldap030.png)

#### Paso 7
Para verificar la importación de los usuarios y configurar sus respectivos roles, seleccione la opción _Usuarios_.

![](images/ldap031.png)

> _Ultima Modificación al manual: 19/5/2022_ 
> @JhonnyMonrroy
 