# MANUAL DE USUARIO - PERFIL ADMINISTRADOR
El usuario supervisor puede:

* Hacer seguimiento de solicitudes
* Reasignar a otros tecnicos una solicitud
* Ver estadísticas de tickets solucionados, cerrados y en proceso.
* Crear grupos de usuarios
* Configurar las notificaciones entre otros.
* Crear reglas para la gestión de solicitudes y parametros predefinidos para cada solicitud nueva.
* Crear, dar baja a usuarios.
* Modificar las vistas y plantillas de solicitudes.

## 1. GESTIÓN DEL DOCUMENTO

| | Elaboración | Revisión | Aprobación |
| ---: | :---: | :---: | :---: |
| **Firma** | | | |
| **Nombre** | Jhonny Ruben Monrroy Casillo | Richar Angel Marquez Zacari | Hans Wilmer Choque Gutierrez |
| **Cargo** | __Consultor - Desarrollo de Servicios de Verificación__ | __Jefe de la Unidad Nacional de Implementación de Sistemas__ | __Director Nacional de Tecnologías de la Información__ |
| **Fecha** | 08/06/2022 | | |

## 2. OBJETO DEL DOCUMENTO

Mediante el presente documento se proporciona información necesaria que debe ser tomada en cuenta por el usuario de Perfil Administrador que hará uso del Software GLPI - SEGIP.

## 3. ÁMBITO DE APLICACIÓN

Se aplica a todos los servidores públicos que requieren hacer configuraciones al sistema de solicitudes GLPI administrada por la Unidad de Implementación y Explotación de Aplicaciones Informáticas.

## 4. DEFINICIONES

| Definición | Descripción |
| :---: | :--- |
| GLPI | GLPi  es una solución libre de gestión de servicios de tecnología de la información, un sistema de seguimiento de incidencias solicitudes. Este software de código abierto está editado en PHP y distribuido bajo una licencia GPL. |
| ANS | En el software GLPi un ANS se refiere a la configuración que se realiza para un Acuerdo de Nivel de servicio, este nivel de servicio sirve para activar notificaciones y alertas sobre si un caso esta mucho tiempo en estado de espera o solución. |
| OLA | En el software GLPi un OLA se refiere a la configuración que se realiza para un Acuerdo de Nivel de servicio para la Organización, se recomienda que este acuerdo OLA sea menor en los tiempos asignados en un ANS puesto que este sirve para que a nivel organizacional se tenga una eficiencia en tiempo de respuesta de las solicitudes, este nivel de servicio organizacional sirve para activar notificaciones y alertas sobre si un caso esta mucho tiempo en estado de espera y solución. |

## 5. DOCUMENTOS DE REFERENCIA

| Códido | Título |
| :---: | :---: |
| N/A | |

## 6. MANUAL

### 6.1. INICIO DE SESIÓN

#### Ingresar al sistema GLPI

Utilice su usuario y password de dominio para autenticarse al sistema.

![](images/man_operador001.png)

#### Reconociendo la interfaz de usuario

![](images/man_tecnico001.png)

### 6.2. PERSONALIZACION DE LA INTERFAZ DE USUARIO
Para configurar su entorno de usuario, siga los siguientes pasos:

#### Abrir el menú de usuario
Hacer click en el menu de usuario ubicado en la parte superior derecha del nombre de usuario.

![](images/man_operador004.png)

#### Ubicar el apartado de opciones de usuario

Abrir las opciones de usuario y se desplegará el formulario para personalizar los datos del usuario.

![](images/man_operador005.png)

#### Completar los campos faltantes y guardar los cambios

![](images/man_operador006.png)

#### En la pestaña _Personalización_ se puede personalizar la interfaz de usuario según sus preferencias

![](images/man_operador007.png)

### 6.3. PANEL PRINCIPAL DE SOLICITUDES
En la pantalla de inicio, en la vista personal se listan las solicitudes que se le asignaron al técnico.

![](images/man_tecnico002.png)

En la pestaña de *Vista de grupo* se listan las solicitudes que son asignadas al grupo de tecnicos que perteneces.

![](images/man_tecnico003.png)

En la *Vista Global* se muestran estadísticas de la cantidad de solicitudes nuevas, en curso, resueltas, etc.

![](images/man_tecnico004.png)

En la vista *RSS feeds* se muestra noticias y comunicados del administrador del sistema siempre y cuando esta opción esté habilitada para tu usuario.

![](images/man_tecnico005.png)

### 6.4. MENÚ DE SOPORTE
Este menú muestra opciones para hacer seguimiento a los tickets de atención, crear nuevas solicitudes, ver el listado de problemas, estadísticas entre otros.

#### Ver panel de estadísticas de los tickets
En el menú de soporte elegir la opción "panel".

![](images/man_oficial001.png)

Aparecerá un panel con cuadros con información de la cantidad de Tickets atendidos, peticiones tardías, peticiones entrantes, etc.

![](images/man_oficial002.png)

#### Ver el listado de Tickets

Para ver el listado de tickets haga click en el menú *Soporte>Tickets*

![](images/man_tecnico007.png)

Paso seguido se listará los tickets en orden cronológico

![](images/man_tecnico008.png)

#### Ver el flujo de un Ticket

En el listado de tickets, hacer click en la solitud a verificar.

![](images/man_tecnico009.png)

Se puede observar el estado de la solicitud y técnicos asociados y otra información.

![](images/man_tecnico010.png)

### 6.5. CREAR GRUPOS DE USUARIOS
Hacer click en la opción *"Groups"* del menu *Administración*

![](images/man_supervisor001.png)

Si se desea crear un nuevo grupo, hacer click en el botón "Añadir".

![](images/man_supervisor002.png)

Completar los datos del cuadro de diálogos y presionar el botón "Añadir".

![](images/man_supervisor003.png)

### 6.6. ADICIONAR USUARIOS A UN GRUPO

Hacer click en la opción *"Usuarios"* del menú *Administración*.

![](images/man_supervisor004.png)

Seleccionar a los usuarios que se desea adicionar al grupo.

![](images/man_supervisor005.png)

Con los usuarios seleccionados, hacer click en el botón acciones.

![](images/man_supervisor006.png)

En el cuadro de dialogo de acciones elegir "Asociar a un grupo", Seleccionar el Grupo y hacer click en el boton *"Añadir"*.

![](images/man_supervisor007.png)

Los usuarios ahora estarán asociados al grupo seleccionado.

![](images/man_supervisor008.png)

### 6.7. CONFIGURAR NOTIFICACIONES
Es posible configurar el tipo de notificaciones que se desea que el GLPI emita.

Para configurar las notificaciones haga clic en el menú *Configuración>Notificaciones*

![](images/man_admin001.png)

En el panel de notificaciones usted puede habilitar las notificaciones por correo electrónico y notificaciones desde el navegador.

![](images/man_admin002.png)

> ***Nota.-** Para que el usuario reciba las notificaciones en su navegador, es necesario que se tenga activada esta opción en el equipo del usuario, además de que el sitio tenga conexión segura mediante certificados SSL*
> 
> ![](images/man_admin003.png)

#### 6.7.1. PERSONALIZAR NOTIFICACIONES

Puede personalizar sus notificaciones en las plantillas de Notificaciones.

![](images/man_admin004.png)

En este apartado tiene opción a modificar una plantilla o crear una nueva.

![](images/man_admin005.png)

Completar los campos solicitados y presione el botón "Añadir" o "Guardar" según corresponda.

![](images/man_admin006.png)

Paso seguido aparece la opción de personalizar las notificaciones enviadas al correo electrónico, cambie los parámetros según su criterio y haga click en "Añadir" o "Guardar" según corresponda.

![](images/man_admin007.png)

#### 6.7.2.	ACTIVAR NOTIFICACIONES

Para activar o desactivar las notificaciones haga clic en la opción notificaciones.

![](images/man_admin008.png)

En el listado de notificaciones elija el que quiere personalizar.

![](images/man_admin009.png)

Campos para activar y configurar los eventos en los cuales se debe activar esta notificación.

![](images/man_admin010.png)

Se puede ver un listado de las plantillas asociadas a la notificación.

![](images/man_admin011.png)

Se puede seleccionar a los usuarios a quien se quiere enviar la notificación.

![](images/man_admin012.png)

#### 6.7.3. CONFIGURACIÓN DE LOS PARÁMETROS PARA ENVIÓ DE NOTIFICACIONES POR CORREO ELECTRÓNICO

Para configurar las notificaciones mediante correo electrónico, haga clic en la opción "Configuración de los seguimientos por correo".

![](images/man_admin013.png)

Complete los campos solicitados según su administrador del servidor de correo electrónico.

![](images/man_admin014.png)

#### 6.7.4. CONFIGURACIÓN DE LAS NOTIFICACIONES DEL NAVEGADOR

Es posible enviar notificaciones al usuario del sistema mediante el navegador, para configurar esta haga click en la opción "Configuración de seguimiento de navegador" del menú Notificaciones.

![](images/man_admin015.png)

Puede probar si su navegador es compatible con las notificaciones haciendo click en el botón de enviar notificación de prueba.

![](images/man_admin016.png)

Si el navegador lo soporta se mostrará un mensaje en la parte inferior de su pantalla.

![](images/man_admin017.png)

> ***Nota.-** Para que el usuario reciba las notificaciones en su navegador, es necesario que se tenga activada esta opción en el navegador del propio usuario, además de que el sitio tenga conexión segura mediante certificados SSL*
> 
> ![](images/man_admin003.png)

### 6.8. CONFIGURAR CALENDARIOS

Para medir los tiempos de respuesta y atención de las solicitudes es posible crear calendarios (Horario de trabajo) para los cálculos en tiempo de atención.

Haga click en el menú *Configuración>Desplegables*

![](images/man_admin018.png)

Elija la opción *Calendarios*

![](images/man_admin019.png)

Se tiene 2 opciones 

* Calendars
    * Horarios en los que los técnicos están disponibles
* Close times
    * Horarios en donde los técnicos no pueden atender (feriados, etc.)

Elegir la opción *Calendars*

![](images/man_admin020.png)

Puede editar un horario o crear uno nuevo.

![](images/man_admin021.png)

En *Times ranges* puede definir los días y horarios activos de los técnicos

![](images/man_admin022.png)

En *Close times* puede definir los feriados o dias que no se atenderá para no contabilizarlos en los tiempos de atención.

![](images/man_admin023.png)

### 6.9	CONFIGURAR NIVELES DE ATENCIÓN

Al momento de generar una solicitud se puede asignar un tiempo de atención según el nivel de urgencia de una solicitud.

Estos parámetros pueden ser configurados en el Menú *Configuración>Niveles de servicio*.

![](images/man_admin024.png)

Puede adicionar un nuevo nivel o editar una existente.

![](images/man_admin025.png)

En cada nivel de servicio se debe seleccionar el calendario en el cual se basará estos cálculos de tiempo de atención.

![](images/man_admin026.png)

Los acuerdos de servicio con el cliente (Los que se mostrarán en el flujo de solicitud del cliente) pueden ser configurados en la opción *ANS*

![](images/man_admin027.png)

Cada Acuerdo de Servicio ANS puede definir un tiempo de respuesta o atención según lo que se quiera mostrar al usuario.

![](images/man_admin028.png)

Los acuerdos de servicio organizacional (Estos no se muestran al cliente, sirven para medir la eficacia de atención de los técnicos y categorizar la prioridad de las solicitudes entrantes) pueden ser configurados en la opción *OLA*.

![](images/man_admin029.png)

Cada Acuerdo Organizacional OLA puede definir un tiempo de respuesta o atención. (Frecuentemente el tiempo de respuesta o atención organizacional es menor al acuerdo de atención con el cliente)

![](images/man_admin030.png)

### 6.10. CONFIGURAR REGLAS DE NEGOCIO

Es posible crear reglas que permitan la auto asignación de tiempos de solicitud y técnicos según los tipos y categorías de las solicitudes, estos son como observadores que realizan acciones siempre y cuando se cumplan algunos criterios de las solicitudes.

Para configurar las reglas de negocios haga click en el menú *Administración>Rules*

![](images/man_admin031.png)

Elija la opción *Reglas de negocio para peticiones*.

![](images/man_admin032.png)

Puede editar una regla o adicionar una nueva regla.

![](images/man_admin033.png)

Con su interfaz puede activar y especificar cuando aplicar esta regla.

![](images/man_admin034.png)

Se puede especificar los criterios que debe seguir esta regla para activar una acción.

![](images/man_admin035.png)

Se puede activar las acciones que se realizarán al cumplirse algunos criterios de una solicitud.

![](images/man_admin036.png)

Para probar que reglas se cumplen cuando se dan algunas condiciones, debe listar las reglas y presionar el boton *Probar el motor de reglas*

![](images/man_admin037.png)

Especificar qué criterios evaluar.

![](images/man_admin038.png)

A continuación, se listan las reglas qué se activan y que criterios se evaluaron.

![](images/man_admin039.png)

## 7. REFERENCIAS

* TECLIB. (2022). GLPI Project. Obtenido de <https://glpi-project.org/es/>

## 8. REGISTROS

| Código | Título |
| :---: | :---: |
| N/A | |

## 9. CONTROL DE CAMBIOS

| Versión | Fecha de modificación | Cambios de modificación |
| :---: | :---: | :--- |
| 00 | 08/06/2022 | Versión inicial del documento @jhonny.monrroy |

