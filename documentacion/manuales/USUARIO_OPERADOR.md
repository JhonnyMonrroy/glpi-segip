# MANUAL DE USUARIO - PERFIL OPERADOR

El usuario operador puede:
* Generar solicitudes
* Hacer seguimiento a las solictudes del grupo o globales

## 1. GESTIÓN DEL DOCUMENTO

| | Elaboración | Revisión | Aprobación |
| ---: | :---: | :---: | :---: |
| **Firma** | | | |
| **Nombre** | Jhonny Ruben Monrroy Casillo | Richar Angel Marquez Zacari | Hans Wilmer Choque Gutierrez |
| **Cargo** | __Consultor - Desarrollo de Servicios de Verificación__ | __Jefe de la Unidad Nacional de Implementación de Sistemas__ | __Director Nacional de Tecnologías de la Información__ |
| **Fecha** | 06/06/2022 | | |

## 2. OBJETO DEL DOCUMENTO

Mediante el presente documento se proporciona información necesaria que debe ser tomada en cuenta por el usuario de Perfil Operador que hará uso del Software GLPI - SEGIP.

## 3. ÁMBITO DE APLICACIÓN

Se aplica a todos los servidores públicos que requieren hacer solicitudes de atención a la Unidad de Implementación y Explotación de Aplicaciones Informáticas.

## 4. DEFINICIONES

| Definición | Descripción |
| :---: | :--- |
| GLPI | GLPi  es una solución libre de gestión de servicios de tecnología de la información, un sistema de seguimiento de incidencias solicitudes. Este software de código abierto está editado en PHP y distribuido bajo una licencia GPL. |

## 5. DOCUMENTOS DE REFERENCIA

| Códido | Título |
| :---: | :---: |
| N/A | |

## 6. MANUAL

### 6.1. INICIO DE SESIÓN

#### Ingresar al sistema GLPI

Utilice su usuario y password de dominio para autenticarse al sistema.

![](images/man_operador001.png)

#### Reconociendo la interfaz de usuario

![](images/man_operador002.png)

### 6.2. PERSONALIZACION DE LA INTERFAZ DE USUARIO
Para configurar su entorno de usuario, siga los siguientes pasos:

#### Abrir el menú de usuario
Hacer click en el menu de usuario ubicado en la parte superior derecha del nombre de usuario.

![](images/man_operador004.png)

#### Ubicar el apartado de opciones de usuario

Abrir las opciones de usuario y se desplegará el formulario para personalizar los datos del usuario.

![](images/man_operador005.png)

#### Completar los campos faltantes y guardar los cambios

![](images/man_operador006.png)

#### En la pestaña _Personalización_ se puede personalizar la interfaz de usuario según sus preferencias

![](images/man_operador007.png)

### 6.3. CREAR UNA SOLICITUD DE ATENCIÓN
Para crear y generar una solicitud (Ticket de atención) es necesario seguir los siguientes pasos:

#### En el menú principal seleccionar la opción _"Crear una petición"_

![](images/man_operador008.png)

#### Completar la información de la solicitud

Completar los campos solicitados, es posible adicionar imágenes y archivos adjuntos a la solicitud.

![](images/man_operador009.png)

El sistema le mostrará una notificación de que la solicitud ya fue enviada para su revisión.

![](images/man_operador010.png)

### 6.4. VER EL ESTADO DE UNA SOLICITUD
#### En el menú principal seleccionar la opción _"Tickets"_

![](images/man_operador011.png)

#### En el listado de solicitudes hacer clic en la solicitud a verificar.

![](images/man_operador012.png)

#### Puede revisar el estado de su solicitud y ver detalles del mismo.

![](images/man_operador013.png)

### 6.5. VER EL ESTADÍSTICAS DE SUS SOLICITUDES
Si desea conocer las estadísticas de sus solicitudes haga click en la opción inicio de su menú principal.

![](images/man_operador014.png)

A continuación, se muestra la cantidad de solicitudes nuevas, en curso, resueltas, etc.

![](images/man_operador015.png)

## 7. REFERENCIAS

* TECLIB. (2022). GLPI Project. Obtenido de <https://glpi-project.org/es/>

## 8. REGISTROS

| Código | Título |
| :---: | :---: |
| N/A | |

## 9. CONTROL DE CAMBIOS

| Versión | Fecha de modificación | Cambios de modificación |
| :---: | :---: | :--- |
| 00 | 07/06/2022 | Versión inicial del documento @jhonny.monrroy |
