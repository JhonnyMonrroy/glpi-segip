# MANUAL DE USUARIO - PERFIL TECNICO
El usuario tecnico puede:
* Generar solicitudes
* Hacer seguimiento a las solictudes del grupo o globales
* Poner prioridad de atención a las solcitudes que le fueron asignadas
* Derivar una solictud a otros tecnicos.

## 1. GESTIÓN DEL DOCUMENTO

| | Elaboración | Revisión | Aprobación |
| ---: | :---: | :---: | :---: |
| **Firma** | | | |
| **Nombre** | Jhonny Ruben Monrroy Casillo | Richar Angel Marquez Zacari | Hans Wilmer Choque Gutierrez |
| **Cargo** | __Consultor - Desarrollo de Servicios de Verificación__ | __Jefe de la Unidad Nacional de Implementación de Sistemas__ | __Director Nacional de Tecnologías de la Información__ |
| **Fecha** | 07/06/2022 | | |

## 2. OBJETO DEL DOCUMENTO

Mediante el presente documento se proporciona información necesaria que debe ser tomada en cuenta por el usuario de Perfil Técnico que hará uso del Software GLPI - SEGIP.

## 3. ÁMBITO DE APLICACIÓN

Se aplica a todos los servidores públicos que requieren hacer solicitudes de atención a la Unidad de Implementación y Explotación de Aplicaciones Informáticas.

## 4. DEFINICIONES

| Definición | Descripción |
| :---: | :--- |
| GLPI | GLPi  es una solución libre de gestión de servicios de tecnología de la información, un sistema de seguimiento de incidencias solicitudes. Este software de código abierto está editado en PHP y distribuido bajo una licencia GPL. |

## 5. DOCUMENTOS DE REFERENCIA

| Códido | Título |
| :---: | :---: |
| N/A | |

## 6. MANUAL

### 6.1. INICIO DE SESIÓN

#### Ingresar al sistema GLPI

Utilice su usuario y password de dominio para autenticarse al sistema.

![](images/man_operador001.png)

#### Reconociendo la interfaz de usuario

![](images/man_tecnico001.png)

### 6.2. PERSONALIZACION DE LA INTERFAZ DE USUARIO
Para configurar su entorno de usuario, siga los siguientes pasos:

#### Abrir el menú de usuario
Hacer click en el menu de usuario ubicado en la parte superior derecha del nombre de usuario.

![](images/man_operador004.png)

#### Ubicar el apartado de opciones de usuario

Abrir las opciones de usuario y se desplegará el formulario para personalizar los datos del usuario.

![](images/man_operador005.png)

#### Completar los campos faltantes y guardar los cambios

![](images/man_operador006.png)

#### En la pestaña _Personalización_ se puede personalizar la interfaz de usuario según sus preferencias

![](images/man_operador007.png)

### 6.2. PANEL PRINCIPAL DE SOLICITUDES
En la pantalla de inicio, en la vista personal se listan las solicitudes que se le asignaron al técnico.

![](images/man_tecnico002.png)

En la pestaña de *Vista de grupo* se listan las solicitudes que son asignadas al grupo de tecnicos que perteneces.

![](images/man_tecnico003.png)

En la *Vista Global* se muestran estadísticas de la cantidad de solicitudes nuevas, en curso, resueltas, etc.

![](images/man_tecnico004.png)

En la vista *RSS feeds* se muestra noticias y comunicados del administrador del sistema siempre y cuando esta opción esté habilitada para tu usuario.

![](images/man_tecnico005.png)

### 6.4. MENÚ DE SOPORTE
Este menú muestra opciones para hacer seguimiento a los tickets de atención, crear nuevas solicitudes, ver el listado de problemas, estadísticas entre otros.

#### Ver el listado de Tickets

Para ver el listado de tickets haga click en el menú *Soporte>Tickets*

![](images/man_tecnico007.png)

Paso seguido se listará los tickets en orden cronológico

![](images/man_tecnico008.png)

#### Ver el flujo de un Ticket

En el listado de tickets, hacer click en la solitud a verificar.

![](images/man_tecnico009.png)

Se puede observar el estado de la solicitud y técnicos asociados y otra información.

![](images/man_tecnico010.png)

### 6.5. AUTO-ASIGNARSE UNA SOLICITUD
Cuando una solicitud no es atendida, es posible asignarse este ticket para que usted lo atienda. Para esto siga los siguientes pasos:

#### Elegir un ticket que este en estado nuevo y no tenga un técnico asignado

![](images/man_tecnico011.png)

#### En el panel derecho, en el apartado de *Asignada a*, hacer click en el icono de usuario

![](images/man_tecnico012.png)

#### Hacer click en guardar para que la asignación sea consolidada y pueda atender el caso

![](images/man_tecnico013.png)

### 6.6. ATENDER UNA SOLICITUD
Para atender una solicitud asignada, siga los siguientes pasos:

#### Abrir una solicitud 
En el panel inicial hacer click en la solicitud a responder.

![](images/man_tecnico014.png)

#### Responder una solicitud
Tiene varias opciones para responder una solicitud.

![](images/man_tecnico015.png)

#### 6.6.1. ENVIAR UN MENSAJE PERSONALIZADO AL SOLICITANTE
Puede enviar un mensaje al solicitante haciendo click directamente en el botón *Responder*

![](images/man_tecnico016.png)

Complete los campos solicitados y haga click en el botón *Añadir*

![](images/man_tecnico017.png)

El mensaje quedará registrado en el flujo de la solicitud

![](images/man_tecnico018.png)

### 6.6.2. CREAR UNA TAREA AL ATENDER UNA SOLICITUD
Dentro del flujo de atención de una solicitud se puede generar un mensaje marcado como tarea a ser asignada a otro técnico u a uno mismo.

Haga click en la opción *Create a task* de la opción Responder.

![](images/man_tecnico019.png)

Complete los campos solicitados y haga click en el botón *Añadir*

![](images/man_tecnico020.png)

La tarea aparecerá en el flujo de la solicitud y esta cambiará al estado de "en curso (planificada)".

![](images/man_tecnico021.png)

### 6.6.3. CREAR UNA PREGUNTA DE VALIDACIÓN

En el botón "Responder" haga click en la opción *Ask for validation*

![](images/man_tecnico022.png)

Completar los campos solicitados y hacer click en la opcion *Añadir*

![](images/man_tecnico023.png)

El mensaje de confirmación aparecerá en el flujo de la solicitud.

![](images/man_tecnico024.png)

> En la interfaz del que valida la solicitud, aparece un mensaje con las opciones "Aprobar" y "Rechazar".
> ![](images/man_tecnico025.png)

Una vez que el mensaje de validación fue aceptado, en el flujo de la solicitud aparece el mensaje de "Validación Concedida o Rechazada"

![](images/man_tecnico026.png)

#### 6.6.4. MANDAR UN MENSAJE DE SOLUCIÓN
En el botÓn "Responder" haga click en la opción *Añade una solución*

![](images/man_tecnico028.png)

Llenar los campos solicitados y presionar el botón "Añadir"

![](images/man_tecnico029.png)

> **Nota.-** Al realizar este paso la solicitud cambiara al estado de "Resuelto"

### 6.7. CREAR UNA SOLICITUD DE ATENCIÓN
El usuario Técnico también puede crear una solicitud. Para crear y generar una solicitud (Ticket de atención) es necesario seguir los siguientes pasos:

#### En el menú principal seleccionar la opción _"Crear una petición"_

![](images/man_operador008.png)

#### Completar la información de la solicitud

Completar los campos solicitados, es posible adicionar imágenes y archivos adjuntos a la solicitud.

![](images/man_operador009.png)

El sistema le mostrará una notificación de que la solicitud ya fue enviada para su revisión.

![](images/man_operador010.png)

### 6.8. VER EL ESTADO DE UNA SOLICITUD
#### En el menú principal seleccionar la opción _"Tickets"_

![](images/man_operador011.png)

#### En el listado de solicitudes hacer clic en la solicitud a verificar.

![](images/man_operador012.png)

#### Puede revisar el estado de su solicitud y ver detalles del mismo.

![](images/man_operador013.png)

## 7. REFERENCIAS

* TECLIB. (2022). GLPI Project. Obtenido de <https://glpi-project.org/es/>

## 8. REGISTROS

| Código | Título |
| :---: | :---: |
| N/A | |

## 9. CONTROL DE CAMBIOS

| Versión | Fecha de modificación | Cambios de modificación |
| :---: | :---: | :--- |
| 00 | 08/06/2022 | Versión inicial del documento @jhonny.monrroy |
