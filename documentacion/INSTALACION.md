# Este manual es para instalar el GLPI modificado para el SEGIP
**Objetivo.-** crear un método para la actualización del sistema, como configuración inicial del proceso.

## Configuración inicial

### Paso 1
Luego de levatar el sistema base en el host se procede al procedimiento de instalación y configuración de la base de datos.

Elegir el lenguaje de instalación y presionar el botón **correcto**.

![](images/inst001.png)

### Paso 2
>	Este software esta en base al proyecto https://github.com/glpi-project/glpi por lo que se rige bajo la licencia GPLv3.
Es necesario aceptar el acuerdo de licencia antes de instalar el sistema.

![](images/inst002.png)

### Paso 3
Si es la primera vez que instala el software haga clic en **Instalar**.

![](images/inst003.png)

### Paso 4
Se procedera a verificar si tiene las extensiones habilitadas para que el software se ejecute correctamente.
Instale las extenciones requeridas en su host y haga clic en **Continuar**
![](images/inst004.png)
> **Nota.-** Si procedio a la habilitación de requerimientos es necesario verificar que no hay problemas con la instalación, para esto es necesario presionar el boton ** Intentalo de nuevo** para verificar estos cambios antes de continuar.

![](images/inst005.png)

### Paso 5 
Si todo salio bien se procedera a desplegarse una pantalla para la configuración y conexión de base de datos.

![](images/inst006.png)

## Instalación de la Base de datos
El software *GLPI* trabaja con el gestor de base de datos *MySQL* compatible con *MariaDB* 
### Paso 1
Igresar con sus credenciales de MySQL en el servidor de MySQL.

```bash
mysql -u <username> -p
```
### Paso 2
Dentro de la interfaz de MySQL se debe adicionar al usuario que hara uso del sistema.

```sql
CREATE USER 'glpi_user'@'%' IDENTIFIED BY '<password>';
```

### Paso 3
Crear la base de datos
```sql
CREATE DATABASE glpi_db CHARACTER SET utf8;
```

### Paso 4
Se le asigna los correspondientes permisos al usuario para acceder a la base de datos.
```sql
GRANT ALL ON glpi_db.* TO 'glpi_user'@'%';

FLUSH PRIVILEGES;
```

## Creación de tablas y migraciones
### Paso 1
Una vez creado el usuario y la base de datos se procede a llenar los campos solicitados en la Instalación del GLPI, y se pulsa en el boton **Continuar**.

![](images/inst007.png)

### Paso 2
Seleccionar la base de datos creada anterior mente y presionar el boton **Continuar**.

![](images/inst008.png)

### Paso 3
Si todo salio bien nos mostrará un mensaje de que la base de datos ha sido inicializada. Presionar el botón **Continuar**.

![](images/inst009.png)

### Paso 4
La configuración inicial ya esta completada... presiona el boton "Continuar".

![](images/inst011.png)

### Paso 5
Si es la primera vez que se instala el sistema en este apartado aparacerá las credenciales predeterminadas para que puedas utilisar el sistema. Hacer click en **Utilizar GLPI**.

![](images/inst012.png)

### Paso 6
Ahora puede authenticarse en el sistema...

![](images/inst013.png)

## Acceso a los usuarios de un _LDAP_
Es posible importar a los usuarios de un LDAP. Para ese fin consultar el documento [LDAP.md](LDAP.md).

## Configuraciones adicionales
Es necesario realizar algunas configuraciones adicionales coo ser corresos, zonas horarias y otras. Para realizar estas configuraciones siga los pasos descritos en el doucmento [CONFIGURACION.md](CONFIGURACION.md).

> Ultima Modificación del manual 20/5/2022
> @JhonyMonrroy
