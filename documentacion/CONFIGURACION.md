# CONFIGURACIONES
## Configuración de la zona horaria
### Paso 1
Con el usuario administrador ingresar al menu _Configuración>General_

![](images/conf001.png)

### Paso 2
En el panel de configuración general seleccionar la opción _Valores Predeterminados_, luego buscar el campo donde se configura la zona horaria.

![](images/conf002.png)

> __Nota.-__ Si es una instalación nueva es necesario que se active la zona de horario en la instalación, para este fin ejecute el siguiente comando en la carpeta de instalación del glpi
>
> ```bash
> php bin/console glpi:database:enable_timezones
> ```
> Si se tiene algun error del tipo:
>
> ![](images/conf_err001.png)
> 
> debe asegurarse de que las zonas horarias de su sistema estan activas, ademas que el usuario de la base del glpi tenga acceso a la tabla mysql.time_zone. (Para instalaciones _Windows_ se debe seguir los pasos descritos en la documentación del GLPI <https://glpi-install.readthedocs.io/en/latest/timezones.html>)
>
> ```bash
> mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -p -u root mysql
> mysql -u root -p
> ```
>
> ```sql
> GRANT SELECT ON `mysql`.`time_zone_name` TO 'glpi_user'@'%';
> FLUSH PRIVILEGES;
> exit
> ```
> Y volver a ejecutar el comando:
>
> ```bash
> php bin/console glpi:database:enable_timezones
> ```
> Si aparece un error de tipo:
>
> ![](images/conf_err002.png)
>  
> ejecute el siguiente comando adicional.
> ```bash
> php bin/console glpi:migration:timestamps
> php bin/console glpi:database:enable_timezones
> ```

Colocar la zona horaria segun corresponda y guarde el cambio.

![](images/conf003.png)

## Configuración de las Notificaciones de seguimiento por correo
Se puede habilitar las notificaciones sobre los tickets siguiendo los siguientes pasos:
### Paso 1
Con el usuario administrador seleccionar la opción _Configuración>Notificaciones_

![](images/conf004.png)
### Paso 2
Activar las opciones de seguimiento
![](images/conf005.png)

### Paso 3
Activar los seguimientos que se quieren activar
![](images/conf006.png)

### Paso 4
Seleccionar la opción de configuración de seguimientos por correo

![](images/conf007.png)
### Paso 5
Llenar los campos solicitados y probar la conexión haciendo click en _Enviar un correo de prueba al administrador_

![](images/conf008.png)

### Paso 6
Si la conexión tuvo exito aparecerá un mensaje de confirmación

![](images/conf009.png)

> __Nota.-__ Los receptores aun no estan configurados para capturar ticets desde el propio email.

~~Los receptores aun no estan habilitados para Emails entrantes~~

> __Ultima modificación:__ 24/05/2022 @JhonnyMonrroy

