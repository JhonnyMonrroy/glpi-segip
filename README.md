# MANUAL DE INSTALACIÓN
## Requerimientos preliminales
> Este proceso se verificó en un entorno Linux con distribución Debian 10

### Instalación del servidor web Apache

#### Paso 1

Actualizar el repositorio interno del sistema operativo

```bash
sudo apt update
```
#### Paso 2

Instalar el servidor apache 2

```bash
sudo apt install apache2
```
#### Paso 3

Para verificar que el servidor web esta en line puede utilizar el comando
```bash
ss -lntu
```
*Ejempo de salida*

    Netid        State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port
    tcp          LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
    tcp          LISTEN        0             128                              *:80                            *:*
    tcp          LISTEN        0             128                           [::]:22                         [::]:*


y verificar que el puerto 80 de su servidor esta activo

### Instalación del PHP
#### Paso 1
Se debe actualizar la lista de repositorios con el siguiente comando:

```bash
sudo apt update
```
#### Paso 2
Agregar los repositorios de PPA de Sury.

```bash
sudo apt -y install lsb-release apt-transport-https ca-certificates 
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
```

### Paso 3
Agregar el repositorio a la lista de repositorios del servidor, seguido esto actualizamos la lista de paquetes.

```bash
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

sudo apt update
```

### Paso 4
Proceder a instalar Php en el sistema
```bash
sudo apt install php
```

### Paso 5
Verificar la versión del PHP instalada

```bash
php -v
```

*Ejempo de salida*

    PHP 8.1.5 (cli) (built: Apr 22 2022 04:54:30) (NTS)
    Copyright (c) The PHP Group
    Zend Engine v4.1.5, Copyright (c) Zend Technologies
        with Zend OPcache v8.1.5, Copyright (c), by Zend Technologies

### Paso 6
Instalar las extensiones necesarias para el sistema

```bash
sudo apt install php-json

sudo apt install php-curl
sudo apt install php-gd
sudo apt install php-intl
sudo apt install php-mysqli
sudo apt install php-fileinfo
sudo apt install php-dom
sudo apt install php-simplexml
sudo apt install php-opcache
sudo apt install php-imap

sudo apt install php-mbstring 
```

> Opcionalmente si se hara una conexión a un servidor LDAP sera necesario habilitar la extensión ldap
>
>```bash
>sudo apt install php-ldap
>```

> **Nota.-** Es posible que para algunas extensiones se tenga que elegir una version de la extención a instalar como por ejemplo para la extensión *"mysqli"* se puede solicitar instalar el paquete *"php8.1-mysqli"* dependiendo de la version de PHP que tenga instalada en el equipo.

### Paso 7
Reiniciar el servidor apache.

```bash
sudo service apache2 restart
```
### Instalación del servidor de base de datos MariaDB

#### Paso 1
Actualizar los repositorios
```bash
sudo apt update
```

#### Paso 2
Instalar mariaDB a partir del repositorio oficial
```bash
sudo apt install mariadb-server
```
#### Paso 3
Para verificar que el servicio mariadb este levantado, ejecuta el siguiente comando:
```bash
systemctl status mariadb
```

*Ejempo de salida*

    ● mariadb.service - MariaDB 10.3.34 database server
    Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
    Active: active (running) since Tue 2022-05-17 09:27:16 -04; 9min ago
        Docs: man:mysqld(8)
            https://mariadb.com/kb/en/library/systemd/
    Main PID: 13634 (mysqld)
    Status: "Taking your SQL requests now..."
        Tasks: 31 (limit: 4915)
    Memory: 76.7M
    CGroup: /system.slice/mariadb.service
            └─13634 /usr/sbin/mysqld

#### Paso 4
Activar el inicio de sesión por usuario y contaseña
```bash
sudo mysql
```
En la consola de MariaDB ejecutar

```sql
update mysql.user set plugin='' where user='root';

alter user root@localhost identified by 'XXXXXXXX';

flush privileges;

exit
```
#### Paso 5
Verificar que se puede conectar por contraseña con el servidor MariaDB

```bash
mysql -u root -p
```

*Ejemplo de salida*

    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 40
    Server version: 10.3.34-MariaDB-0+deb10u1 Debian 10

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]>

Salir del servidor

```sql
exit
```

### Copiar el código del sistema

#### Paso 1
Copiar el archivo *glpi.tar* al sistema.

> **Nota.-** Copiar el codigo del sistema *GLPI* segun corresponda (Se sugiere usar un repositorio GIT para futuras versiones)

Luego en el lugar de la descarga ejecutar el comando para descomprimir los archivos

```bash
tar -xvf glpi.tar
```

#### Paso 2
Mover la carpeta descomprimida al ditectorio */var/www/* del Apache2.

```bash
sudo mv glpi /var/www/
```

### Paso 3
Poner como propietario al usuario ROOT para que apache tenga accesos de lectura y escritura
```bash
sudo chown -R root:root /var/www/glpi
```
Asignar permisos de lectura y escritura

```bash
sudo chmod 647 /var/www/glpi/ -R
```

#### Paso 4
Modificar el archivo */etc/apache2/sites-available/000-default.conf* para redireccionar a la pagina principal al sistema glpi.

```bash
sudo nano /etc/apache2/sites-available/000-default.conf
```

Buscar el apartado *DocumentRoot /var/www/html* y cambiarlo por **_DocumentRoot /var/www/glpi_**

```ini
. . .
    # However, you must set it for any further virtual host explicitly.
    #ServerName www.example.com

    ServerAdmin webmaster@localhost
    # DocumentRoot /var/www/html
    DocumentRoot /var/www/glpi

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.

. . .
```

Cerrar y Guardar el archivo con *Ctrl + o*  y  *Ctrl + x*.

#### Paso 5
Reiniciar el servidor apache.

```bash
sudo service apache2 restart
```

El sistema se deberia desplegar correctamente.

![](documentacion/images/inst001.png)

## Configuración del sistema _GLPI_
Una vez levantado la pagina inicial en el servidor apache se debe configurar la base de datos para que el sistema cree las tablas y registros correspondientes. Para ese fin consultar el documento [INSTALACION.md](documentacion/INSTALACION.md).

## MANUALES DE USUARIO

* [USUARIO OPERADOR](documentacion/manuales/USUARIO_OPERADOR.md).
* [USUARIO TECNICO](documentacion/manuales/USUARIO_TECNICO.md).
* [USUARIO OFICIAL](documentacion/manuales/USUARIO_OFICIAL.md).
* [USUARIO SUPERVISOR](documentacion/manuales/USUARIO_SUPERVISOR.md).
* [USUARIO ADMINISTRADOR](documentacion/manuales/USUARIO_ADMINISTRADOR.md).

> Ultima modificación del manual 31/05/2022 por @JhonnyMonrroy

